var Observable = require("data/observable").Observable;

function getMessage(counter) {
    if (counter <= 0) {
        return "Hoorraaay! You unlocked the NativeScript clicker achievement!";
    } else {
        return counter + " taps left";
    }
}

function createViewModel() {
    var viewModel = new Observable();
    viewModel.counter = 42;
    viewModel.message = getMessage(viewModel.counter);

    var data = JSON.parse('{"accounts":{"1":{"id":"1","name":"vledderamando@gmail.com"},"2":{"id":"2","name":"info@amandovledder.eu"}}}');
    //viewModel.accounts = data.accounts;
    viewModel.accounts = ["vledderamando@gmail.com","info@amandovledder.eu"];
    //viewModel.set("accounts", data.accounts);
    //viewModel.set("user", "vledderamando@gmail.com");

    viewModel.onTap = function() {
        this.counter--;
        this.set("message", getMessage(this.counter));
    }

    return viewModel;
}

exports.createViewModel = createViewModel;