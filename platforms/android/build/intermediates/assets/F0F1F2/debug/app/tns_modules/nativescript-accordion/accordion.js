"use strict";
var color_1 = require("color");
var utils = require("utils/utils");
var common = require("./accordion.common");
var stack_layout_1 = require("ui/layouts/stack-layout");
var proxy_1 = require("ui/core/proxy");
var dependency_observable_1 = require("ui/core/dependency-observable");
var dependency_observable_2 = require("ui/core/dependency-observable");
global.moduleMerge(common, exports);
var Accordion = (function (_super) {
    __extends(Accordion, _super);
    function Accordion() {
        var _this = _super.call(this) || this;
        _this._previousGroup = -1;
        _this._views = [];
        _this.selectedIndexes = [];
        return _this;
    }
    Object.defineProperty(Accordion.prototype, "android", {
        get: function () {
            return this._android;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Accordion.prototype, "_nativeView", {
        get: function () {
            return this._android;
        },
        enumerable: true,
        configurable: true
    });
    Accordion.prototype.addToView = function (view) {
        this._addView(view);
    };
    Accordion.prototype.updateItems = function (oldItems, newItems) {
        if (newItems) {
            if (this._listAdapter) {
                this._listAdapter.notifyDataSetChanged();
            }
        }
    };
    Accordion.prototype._createUI = function () {
        var _this = this;
        this._android = new android.widget.ExpandableListView(utils.ad.getApplicationContext());
        var that = new WeakRef(this);
        if (this.separatorColor) {
            this._android.setDivider(new android.graphics.drawable.ColorDrawable(new color_1.Color(this.separatorColor).android));
            this._android.setDividerHeight(1);
        }
        this._android.setGroupIndicator(null);
        this._android.setOnGroupExpandListener(new android.widget.ExpandableListView.OnGroupExpandListener({
            onGroupExpand: function (groupPosition) {
                var owner = that.get();
                if (!owner.allowMultiple) {
                    owner.selectedIndex = groupPosition;
                    if ((owner._previousGroup != -1) && (groupPosition != owner._previousGroup)) {
                        owner.android.collapseGroup(owner._previousGroup);
                    }
                    owner._previousGroup = groupPosition;
                }
                owner.selectedIndexes.forEach(function (item, index, arr) {
                    if (item === groupPosition) {
                        owner.selectedIndexes.slice(index, 1);
                    }
                    if (index === arr.length) {
                        owner.selectedIndexes.push(groupPosition);
                    }
                });
            }
        }));
        this._android.setOnGroupCollapseListener(new android.widget.ExpandableListView.OnGroupCollapseListener({
            onGroupCollapse: function (groupPosition) {
                var owner = that.get();
                var items = owner.selectedIndexes;
                owner.groupCollapsed(groupPosition);
                owner.selectedIndexes = owner.selectedIndexes.map(function (item) {
                    if (item != groupPosition) {
                        return item;
                    }
                });
            }
        }));
        if (this.selectedIndexes) {
            this.selectedIndexes.forEach(function (item) {
                _this._android.expandGroup(item);
            });
        }
        this._listAdapter = new AccordionListAdapter(this);
        this._android.setAdapter(this._listAdapter);
    };
    Accordion.prototype.addItem = function (view) {
        this.items.push(view);
        this._addView(view);
        this._listAdapter.notifyDataSetChanged();
    };
    Accordion.prototype.refresh = function () {
        this._android.setAdapter(null);
        this._listAdapter = null;
        this._listAdapter = new AccordionListAdapter(this);
        this._android.setAdapter(this._listAdapter);
    };
    Accordion.prototype._eachChildView = function (callback) {
        if (this.items) {
            var i = void 0;
            var length = this.items.length;
            var retVal = void 0;
            for (i = 0; i < length; i++) {
                retVal = callback(this.items[i]);
                if (retVal === false) {
                    break;
                }
            }
        }
    };
    Accordion.prototype.indexChanged = function (index) {
        this.notifyPropertyChange("selectedIndex", index);
    };
    Accordion.prototype.groupCollapsed = function (index) {
        this.notifyPropertyChange("groupCollapsed", index);
    };
    Accordion.prototype.expandItem = function (id) {
        if (id) {
            this._android.expandGroup(id);
        }
    };
    Accordion.prototype.isItemExpanded = function (id) {
        return this._android.isGroupExpanded(id);
    };
    return Accordion;
}(common.Accordion));
exports.Accordion = Accordion;
var headerTextProperty = new dependency_observable_2.Property("headerText", "Item", new proxy_1.PropertyMetadata(undefined, dependency_observable_1.PropertyMetadataSettings.None));
headerTextProperty.metadata.onSetNativeValue = function (data) {
    var item = data.object;
    item.headerTextChanged(data.newValue);
};
var Item = (function (_super) {
    __extends(Item, _super);
    function Item() {
        return _super.call(this) || this;
    }
    Item.prototype.headerTextChanged = function (text) {
    };
    Object.defineProperty(Item.prototype, "headerText", {
        get: function () {
            return this._getValue(Item.headerTextProperty);
        },
        set: function (text) {
            this._setValue(Item.headerTextProperty, text);
        },
        enumerable: true,
        configurable: true
    });
    return Item;
}(stack_layout_1.StackLayout));
Item.headerTextProperty = headerTextProperty;
exports.Item = Item;
var AccordionListAdapter = (function (_super) {
    __extends(AccordionListAdapter, _super);
    function AccordionListAdapter(owner) {
        var _this = _super.call(this) || this;
        _this.owner = owner;
        return global.__native(_this);
    }
    AccordionListAdapter.prototype.getChild = function (groupPosition, childPosition) {
        return null;
    };
    AccordionListAdapter.prototype.getGroup = function (groupPosition) {
        return null;
    };
    AccordionListAdapter.prototype.hasStableIds = function () {
        return true;
    };
    AccordionListAdapter.prototype.getGroupView = function (groupPosition, isExpanded, convertView, parent) {
        if (this.owner.items) {
            var item = this.owner.items[groupPosition];
            if (item.headerText) {
                var header = new android.widget.TextView(this.owner._context);
                header.setText(this.owner.items[groupPosition].headerText);
                if (this.owner.headerTextHorizontalAlignment === "center") {
                    header.setGravity(android.view.Gravity.CENTER_HORIZONTAL);
                }
                else if (this.owner.headerTextHorizontalAlignment === "right") {
                    header.setGravity(android.view.Gravity.RIGHT);
                }
                else if (this.owner.headerTextHorizontalAlignment === "left") {
                    header.setGravity(android.view.Gravity.LEFT);
                }
                if (this.owner.headerTextVerticalAlignment === "center") {
                    header.setGravity(android.view.Gravity.CENTER_VERTICAL);
                }
                else if (this.owner.headerTextVerticalAlignment === "top") {
                    header.setGravity(android.view.Gravity.TOP);
                }
                else if (this.owner.headerTextVerticalAlignment === "bottom") {
                    header.setGravity(android.view.Gravity.BOTTOM);
                }
                if (this.owner.headerHeight) {
                    header.setHeight(this.owner.headerHeight);
                }
                if (this.owner.headerColor) {
                    header.setBackgroundColor(new color_1.Color(this.owner.headerColor).android);
                }
                if (this.owner.headerTextColor) {
                    header.setTextColor(new color_1.Color(this.owner.headerTextColor).android);
                }
                if (this.owner.headerTextSize) {
                    header.setTextSize(this.owner.headerTextSize);
                }
                header.setPadding(this.owner.paddingLeft || this.owner.style.paddingLeft, this.owner.paddingTop || this.owner.style.paddingTop, this.owner.paddingRight || this.owner.style.paddingRight, this.owner.paddingBottom || this.owner.style.paddingBottom);
                return header;
            }
            return null;
        }
    };
    AccordionListAdapter.prototype.getGroupCount = function () {
        return this.owner.items && this.owner.items.length ? this.owner.items.length : 0;
    };
    AccordionListAdapter.prototype.getGroupId = function (groupPosition) {
        return groupPosition;
    };
    AccordionListAdapter.prototype.getChildView = function (groupPosition, childPosition, isLastChild, convertView, parent) {
        if (!this.owner.items[groupPosition].parent) {
            this.owner._addView(this.owner.items[groupPosition]);
        }
        return this.owner.items[groupPosition]._nativeView;
    };
    AccordionListAdapter.prototype.getChildId = function (groupPosition, childPosition) {
        return childPosition;
    };
    AccordionListAdapter.prototype.getChildrenCount = function (groupPosition) {
        return this.owner.items[groupPosition] ? 1 : 0;
    };
    AccordionListAdapter.prototype.isChildSelectable = function (groupPosition, childPosition) {
        return true;
    };
    return AccordionListAdapter;
}(android.widget.BaseExpandableListAdapter));
exports.AccordionListAdapter = AccordionListAdapter;
