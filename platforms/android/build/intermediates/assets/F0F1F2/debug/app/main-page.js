/*
In NativeScript, a file with the same name as an XML file is known as
a code-behind file. The code-behind is a great place to place your view
logic, and to set up your page’s data binding.
*/

/*
NativeScript adheres to the CommonJS specification for dealing with
JavaScript modules. The CommonJS require() function is how you import
JavaScript modules defined in other files.
*/ 
var createViewModel = require("./main-view-model").createViewModel;
var Observable = require("data/observable").Observable;
require( "nativescript-dom" );
function onNavigatingTo(args) {
    /*
    This gets a reference this page’s <Page> UI component. You can
    view the API reference of the Page to see what’s available at
    https://docs.nativescript.org/api-reference/classes/_ui_page_.page.html
    */
    var page = args.object;
    
    /*
    A page’s bindingContext is an object that should be used to perform
    data binding between XML markup and JavaScript code. Properties
    on the bindingContext can be accessed using the {{ }} syntax in XML.
    In this example, the {{ message }} and {{ onTap }} bindings are resolved
    against the object returned by createViewModel().

    You can learn more about data binding in NativeScript at
    https://docs.nativescript.org/core-concepts/data-binding.
    */
    page.bindingContext = createViewModel();
}

/*
Exporting a function in a NativeScript code-behind file makes it accessible
to the file’s corresponding XML file. In this case, exporting the onNavigatingTo
function here makes the navigatingTo="onNavigatingTo" binding in this page’s XML
file work.
*/
exports.onNavigatingTo = onNavigatingTo;

var ExpandableListItem = require("nativescript-expandable-list-view").ExpandableListItem;

var models = [
    {
        id: 1,
        title: "Item A",
        items: [
            {
                id: 1,
                title: 'Sub item A 1'
            },
            {
                id: 2,
                title: 'Sub item A 2'
            },
            {
                id: 3,
                title: 'Sub item A 3'
            },
        ]
    },
    {
        id: 2,
        title: "Item B",
        items: [
            {
                id: 1,
                title: 'Sub item B 1'
            },
            {
                id: 2,
                title: 'Sub item B 2'
            },
            {
                id: 3,
                title: 'Sub item B 3'
            },
            {
                id: 4,
                title: 'Sub item B 4'
            },
        ]
    }   
]

var viewModel

exports.loaded = function(args) {
    var page = args.object;
    var items = []

    for(var i in models){

        var item = new ExpandableListItem({
            id: models[i].id,
            title: models[i].title,
            tag: models[i]
        })          

        for(var j in models[i].items){
            item.addItem(new ExpandableListItem({
                id: models[i].items[j].id,
                title: models[i].items[j].title,
                tag: models[i].items[j]
            }))
        }

        items.push(item)
    }       

    viewModel = new Observable({   
      'items': items 
    })
    //page.bindingContext = viewModel
}

exports.onGroupCollapse = function(){
    console.log("## onGroupCollapse")
}

exports.onGroupExpand = function(){
    console.log("## onGroupExpand")
}

exports.onChildTap = function(args){

    var groupItem = viewModel.get('items')[args.groupIndex]
    var childItem = groupItem.items[args.childIndex]

    alert('Click at group=' + groupItem.title + ', child=' + childItem.title)
}


var ViewModel = require('ui/core/view');

var timer = require("timer");
exports.onPullToRefreshInitiated = function (args) {
    var that = new WeakRef(this);
    timer.setTimeout(function () {
        var listView = args.object;
        listView.notifyPullToRefreshFinished();
    }, 1000);
};

function onCellSwiping(args) {
    var viewModule = require('ui/core/view');
    var utilsModule = require("utils/utils");
    var swipeLimits = args.data.swipeLimits;
    
    var swipeView = args['swipeView'];
    //swipeLimits.left = swipeLimits.right = swipeView.getMeasuredWidth() * 0.45; // 65% of whole width
    swipeLimits.threshold = 60 * utilsModule.layout.getDisplayDensity();
    swipeLimits.left = 80 * utilsModule.layout.getDisplayDensity();
    swipeLimits.right = 80 * utilsModule.layout.getDisplayDensity();
    var mainView = args['mainView'];
    var leftItem = swipeView.getViewById('mark-view');
    var rightItem = swipeView.getViewById('delete-view');
    var leftThresholdPassed = false;
    var rightThresholdPassed = false;
    if (args.data.x > swipeView.getMeasuredWidth() * 0.25 && !leftThresholdPassed) {
        console.log("Notify perform left action");
        var markLabel = leftItem.getViewById('mark-text');
        leftThresholdPassed = true;
    }
    else if (args.data.x < swipeView.getMeasuredWidth() * 0.25 && !rightThresholdPassed) {
        var deleteLabel = rightItem.getViewById('delete-text');
        console.log("Notify perform right action");
        rightThresholdPassed = true;
    }
    if (args.data.x > 0) {
        var leftDimensions = viewModule.View.measureChild(leftItem.parent, leftItem, utilsModule.layout.makeMeasureSpec(Math.abs(args.data.x), utilsModule.layout.EXACTLY), utilsModule.layout.makeMeasureSpec(mainView.getMeasuredHeight(), utilsModule.layout.EXACTLY));
        viewModule.View.layoutChild(leftItem.parent, leftItem, 0, 0, leftDimensions.measuredWidth, leftDimensions.measuredHeight);
    }
    else {
        var rightDimensions = viewModule.View.measureChild(rightItem.parent, rightItem, utilsModule.layout.makeMeasureSpec(Math.abs(args.data.x), utilsModule.layout.EXACTLY), utilsModule.layout.makeMeasureSpec(mainView.getMeasuredHeight(), utilsModule.layout.EXACTLY));
        viewModule.View.layoutChild(rightItem.parent, rightItem, mainView.getMeasuredWidth() - rightDimensions.measuredWidth, 0, mainView.getMeasuredWidth(), rightDimensions.measuredHeight);
    }
}
exports.onCellSwiping = onCellSwiping;