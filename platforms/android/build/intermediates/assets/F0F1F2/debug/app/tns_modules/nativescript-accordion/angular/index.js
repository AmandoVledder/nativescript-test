"use strict";
var core_1 = require("@angular/core");
var element_registry_1 = require("nativescript-angular/element-registry");
var view_1 = require("ui/core/view");
var placeholder_1 = require("ui/placeholder");
var views = [];
var i = 0;
var pagerMeta = {
    skipAddToDom: false,
    insertChild: function (parent, child, index) {
        var accordion = parent;
        var childView = child;
        if (!Array.isArray(accordion.items)) {
            accordion.items = [];
        }
        if (child instanceof placeholder_1.Placeholder) {
        }
        else if (child.nodeName === element_registry_1.TEMPLATE) {
            child.templateParent = parent;
        }
        else if (child.nodeName !== "#text" && child instanceof view_1.View) {
            accordion.items.push(child);
        }
    },
    removeChild: function (parent, child) {
        var accordion = parent;
        var childView = child;
        /*
        if (child.parent === parent) {
          //  parent._nativeView.removeView(child._nativeView)
        //  console.dump(parent._nativeView)
       //   console.dump(child._nativeView.getParent().removeView(child.nativeView()))
           parent._removeView(child);
        }
*/
        /*
        console.log(parent.parent.getChildAt(0))
        console.log(parent.parent.getChildAt(1))
        console.log(parent.parent.getChildAt(2))
        console.log(parent.parent.getChild(3))
        */
        //  accordion._removeView(childView);
        // console.log(accordion._childrenCount)
        // accordion.parent._removeView(accordion);
        // child.r
    }
};
element_registry_1.registerElement("Accordion", function () { return require("../accordion").Accordion; }, pagerMeta);
element_registry_1.registerElement("AccordionItem", function () { return require("../accordion").Item; });
var AccordionComponent = (function () {
    function AccordionComponent() {
    }
    return AccordionComponent;
}());
AccordionComponent = __decorate([
    core_1.Component({
        selector: 'Accordion',
        template: '<ng-content></ng-content>'
    })
], AccordionComponent);
exports.AccordionComponent = AccordionComponent;
// import { ElementRef, Directive, Input, TemplateRef, ViewContainerRef, OnInit, AfterViewInit } from "@angular/core";
// import { Accordion} from "../accordion";
// import {StackLayout} from "ui/layouts/stack-layout";
// @Directive({
//     selector: "Accordion", // tslint:disable-line:directive-selector
// })
// export class AccordionDirective implements AfterViewInit {
//     public accordion: Accordion;
//     private _selectedIndex: number;
//     private viewInitialized: boolean;
//     @Input()
//     get selectedIndex(): number {
//         return this._selectedIndex;
//     }
//     set selectedIndex(value) {
//         // this._selectedIndex = convertToInt(value);
//         if (this.viewInitialized) {
//             this.accordion.selectedIndex = this._selectedIndex;
//         }
//     }
//     constructor(element: ElementRef) {
//         this.accordion = element.nativeElement;
//     }
//     ngAfterViewInit() {
//         this.viewInitialized = true;
//     }
// }
// @Directive({
//     selector: "[accordionItem]" // tslint:disable-line:directive-selector
// })
// export class AccordionItemDirective implements OnInit {
//     private item: AccordionItem;
//     private _headerText: string;
//     constructor(
//         private owner: AccordionDirective,
//         private templateRef: TemplateRef<any>,
//         private viewContainer: ViewContainerRef
//     ) {
//     }
//     @Input("accordionItem") config: any; // tslint:disable-line:no-input-rename
//     @Input()
//     get headerText() {
//         return this._headerText;
//     }
//     set headerText(value: string) {
//         if (this._headerText !== value) {
//             this._headerText = value;
//             console.log(value)
//            // this.ensureItem();
//            // this.item.headerText = this._headerText;
//         }
//     }
//     // @Input()
//     // get iconSource() {
//     //     return this._iconSource;
//     // }
//     // set iconSource(value: string) {
//     //     if (this._iconSource !== value) {
//     //         this._iconSource = value;
//     //         this.ensureItem();
//     //         this.item.iconSource = this._iconSource;
//     //     }
//     // }
//     private ensureItem() {
//         if (!this.item) {
//             this.item = new AccordionItem();
//         }
//     }
//     ngOnInit() {
//        // this.ensureItem();
//         // if (this.config) {
//         //     this.item.title = this._title || this.config.title;
//         //     this.item.iconSource = this._iconSource || this.config.iconSource;
//         // }
//         const viewRef = this.viewContainer.createEmbeddedView(this.templateRef);
//         // Filter out text nodes, etc
//         const realViews = viewRef.rootNodes.filter((node) =>
//             node.nodeName && node.nodeName !== "#text");
//         if (realViews.length > 0) {
//             this.item = realViews[0];
//             //const newItems = (this.owner.accordion.items || []).concat([this.item]);
//             //this.owner.accordion.items = newItems;
//         }
//     }
// }
// export class AccordionItem extends StackLayout {
// } 
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsc0NBQTBDO0FBRTFDLDBFQUF1SDtBQUN2SCxxQ0FBb0M7QUFDcEMsOENBQTZDO0FBSzdDLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQztBQUNmLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztBQUNWLElBQU0sU0FBUyxHQUFrQjtJQUM3QixZQUFZLEVBQUUsS0FBSztJQUNuQixXQUFXLFlBQUMsTUFBVyxFQUFFLEtBQVUsRUFBRSxLQUFhO1FBQzlDLElBQU0sU0FBUyxHQUFtQixNQUFNLENBQUM7UUFDekMsSUFBTSxTQUFTLEdBQVEsS0FBSyxDQUFDO1FBQzdCLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2xDLFNBQVMsQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ3pCLENBQUM7UUFDRCxFQUFFLENBQUMsQ0FBQyxLQUFLLFlBQVkseUJBQVcsQ0FBQyxDQUFDLENBQUM7UUFDbkMsQ0FBQztRQUNELElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsUUFBUSxLQUFLLDJCQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ25DLEtBQUssQ0FBQyxjQUFjLEdBQUcsTUFBTSxDQUFDO1FBQ2xDLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLFFBQVEsS0FBSyxPQUFPLElBQUksS0FBSyxZQUFZLFdBQUksQ0FBQyxDQUFDLENBQUM7WUFDN0QsU0FBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDaEMsQ0FBQztJQUNMLENBQUM7SUFDRCxXQUFXLFlBQUMsTUFBVyxFQUFFLEtBQVU7UUFDL0IsSUFBTSxTQUFTLEdBQW1CLE1BQU0sQ0FBQztRQUN6QyxJQUFNLFNBQVMsR0FBUSxLQUFLLENBQUM7UUFDN0I7Ozs7Ozs7RUFPTjtRQUNNOzs7OztVQUtFO1FBQ0YscUNBQXFDO1FBQ3JDLHdDQUF3QztRQUN4QywyQ0FBMkM7UUFDM0MsVUFBVTtJQUNkLENBQUM7Q0FDSixDQUFDO0FBQ0Ysa0NBQWUsQ0FBQyxXQUFXLEVBQUUsY0FBTSxPQUFBLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxTQUFTLEVBQWpDLENBQWlDLEVBQUUsU0FBUyxDQUFDLENBQUM7QUFDakYsa0NBQWUsQ0FBQyxlQUFlLEVBQUUsY0FBTSxPQUFBLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxJQUFJLEVBQTVCLENBQTRCLENBQUMsQ0FBQztBQUtyRSxJQUFhLGtCQUFrQjtJQUEvQjtJQUNBLENBQUM7SUFBRCx5QkFBQztBQUFELENBQUMsQUFERCxJQUNDO0FBRFksa0JBQWtCO0lBSjlCLGdCQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsV0FBVztRQUNyQixRQUFRLEVBQUUsMkJBQTJCO0tBQ3hDLENBQUM7R0FDVyxrQkFBa0IsQ0FDOUI7QUFEWSxnREFBa0I7QUFNL0Isc0hBQXNIO0FBQ3RILDJDQUEyQztBQUMzQyx1REFBdUQ7QUFDdkQsZUFBZTtBQUNmLHVFQUF1RTtBQUN2RSxLQUFLO0FBQ0wsNkRBQTZEO0FBQzdELG1DQUFtQztBQUNuQyxzQ0FBc0M7QUFDdEMsd0NBQXdDO0FBRXhDLGVBQWU7QUFDZixvQ0FBb0M7QUFDcEMsc0NBQXNDO0FBQ3RDLFFBQVE7QUFFUixpQ0FBaUM7QUFDakMsd0RBQXdEO0FBQ3hELHNDQUFzQztBQUN0QyxrRUFBa0U7QUFDbEUsWUFBWTtBQUNaLFFBQVE7QUFFUix5Q0FBeUM7QUFDekMsa0RBQWtEO0FBQ2xELFFBQVE7QUFFUiwwQkFBMEI7QUFDMUIsdUNBQXVDO0FBQ3ZDLFFBQVE7QUFDUixJQUFJO0FBRUosZUFBZTtBQUNmLDRFQUE0RTtBQUM1RSxLQUFLO0FBQ0wsMERBQTBEO0FBQzFELG1DQUFtQztBQUNuQyxtQ0FBbUM7QUFFbkMsbUJBQW1CO0FBQ25CLDZDQUE2QztBQUM3QyxpREFBaUQ7QUFDakQsa0RBQWtEO0FBQ2xELFVBQVU7QUFDVixRQUFRO0FBRVIsa0ZBQWtGO0FBRWxGLGVBQWU7QUFDZix5QkFBeUI7QUFDekIsbUNBQW1DO0FBQ25DLFFBQVE7QUFFUixzQ0FBc0M7QUFDdEMsNENBQTRDO0FBQzVDLHdDQUF3QztBQUN4QyxpQ0FBaUM7QUFDakMsbUNBQW1DO0FBQ25DLHlEQUF5RDtBQUN6RCxZQUFZO0FBQ1osUUFBUTtBQUVSLGtCQUFrQjtBQUNsQiw0QkFBNEI7QUFDNUIsc0NBQXNDO0FBQ3RDLFdBQVc7QUFFWCx5Q0FBeUM7QUFDekMsK0NBQStDO0FBQy9DLDJDQUEyQztBQUMzQyxvQ0FBb0M7QUFDcEMsMERBQTBEO0FBQzFELGVBQWU7QUFDZixXQUFXO0FBRVgsNkJBQTZCO0FBQzdCLDRCQUE0QjtBQUM1QiwrQ0FBK0M7QUFDL0MsWUFBWTtBQUNaLFFBQVE7QUFFUixtQkFBbUI7QUFDbkIsK0JBQStCO0FBQy9CLGdDQUFnQztBQUNoQyxxRUFBcUU7QUFDckUsb0ZBQW9GO0FBQ3BGLGVBQWU7QUFFZixtRkFBbUY7QUFDbkYsd0NBQXdDO0FBQ3hDLCtEQUErRDtBQUMvRCwyREFBMkQ7QUFDM0Qsc0NBQXNDO0FBQ3RDLHdDQUF3QztBQUV4Qyx5RkFBeUY7QUFDekYsdURBQXVEO0FBRXZELFlBQVk7QUFDWixRQUFRO0FBQ1IsSUFBSTtBQUVKLG1EQUFtRDtBQUVuRCxJQUFJIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IEFjY29yZGlvbiB9IGZyb20gXCIuLi9hY2NvcmRpb25cIjtcbmltcG9ydCB7IHJlZ2lzdGVyRWxlbWVudCwgVmlld0NsYXNzTWV0YSwgTmdWaWV3LCBUZW1wbGF0ZVZpZXcsIFRFTVBMQVRFIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL2VsZW1lbnQtcmVnaXN0cnlcIjtcbmltcG9ydCB7IFZpZXcgfSBmcm9tIFwidWkvY29yZS92aWV3XCI7XG5pbXBvcnQgeyBQbGFjZWhvbGRlciB9IGZyb20gXCJ1aS9wbGFjZWhvbGRlclwiO1xuaW1wb3J0IHsgQnV0dG9uIH0gZnJvbSBcInVpL2J1dHRvblwiO1xuaW1wb3J0IHsgU3RhY2tMYXlvdXQgfSBmcm9tIFwidWkvbGF5b3V0cy9zdGFjay1sYXlvdXRcIjtcbmltcG9ydCAqIGFzIHBsYXRmb3JtIGZyb20gXCJwbGF0Zm9ybVwiO1xuZGVjbGFyZSB2YXIgem9uZWRDYWxsYmFjazogRnVuY3Rpb247XG5sZXQgdmlld3MgPSBbXTtcbmxldCBpID0gMDtcbmNvbnN0IHBhZ2VyTWV0YTogVmlld0NsYXNzTWV0YSA9IHtcbiAgICBza2lwQWRkVG9Eb206IGZhbHNlLFxuICAgIGluc2VydENoaWxkKHBhcmVudDogYW55LCBjaGlsZDogYW55LCBpbmRleDogbnVtYmVyKSB7XG4gICAgICAgIGNvbnN0IGFjY29yZGlvbjogYW55ID0gPEFjY29yZGlvbj5wYXJlbnQ7XG4gICAgICAgIGNvbnN0IGNoaWxkVmlldyA9IDxhbnk+Y2hpbGQ7XG4gICAgICAgIGlmICghQXJyYXkuaXNBcnJheShhY2NvcmRpb24uaXRlbXMpKSB7XG4gICAgICAgICAgICBhY2NvcmRpb24uaXRlbXMgPSBbXTtcbiAgICAgICAgfVxuICAgICAgICBpZiAoY2hpbGQgaW5zdGFuY2VvZiBQbGFjZWhvbGRlcikge1xuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKGNoaWxkLm5vZGVOYW1lID09PSBURU1QTEFURSkge1xuICAgICAgICAgICAgY2hpbGQudGVtcGxhdGVQYXJlbnQgPSBwYXJlbnQ7XG4gICAgICAgIH0gZWxzZSBpZiAoY2hpbGQubm9kZU5hbWUgIT09IFwiI3RleHRcIiAmJiBjaGlsZCBpbnN0YW5jZW9mIFZpZXcpIHtcbiAgICAgICAgICAgIGFjY29yZGlvbi5pdGVtcy5wdXNoKGNoaWxkKTtcbiAgICAgICAgfVxuICAgIH0sXG4gICAgcmVtb3ZlQ2hpbGQocGFyZW50OiBhbnksIGNoaWxkOiBhbnkpIHtcbiAgICAgICAgY29uc3QgYWNjb3JkaW9uOiBhbnkgPSA8QWNjb3JkaW9uPnBhcmVudDtcbiAgICAgICAgY29uc3QgY2hpbGRWaWV3ID0gPGFueT5jaGlsZDtcbiAgICAgICAgLypcbiAgICAgICAgaWYgKGNoaWxkLnBhcmVudCA9PT0gcGFyZW50KSB7XG4gICAgICAgICAgLy8gIHBhcmVudC5fbmF0aXZlVmlldy5yZW1vdmVWaWV3KGNoaWxkLl9uYXRpdmVWaWV3KVxuICAgICAgICAvLyAgY29uc29sZS5kdW1wKHBhcmVudC5fbmF0aXZlVmlldylcbiAgICAgICAvLyAgIGNvbnNvbGUuZHVtcChjaGlsZC5fbmF0aXZlVmlldy5nZXRQYXJlbnQoKS5yZW1vdmVWaWV3KGNoaWxkLm5hdGl2ZVZpZXcoKSkpXG4gICAgICAgICAgIHBhcmVudC5fcmVtb3ZlVmlldyhjaGlsZCk7XG4gICAgICAgIH1cbiovXG4gICAgICAgIC8qXG4gICAgICAgIGNvbnNvbGUubG9nKHBhcmVudC5wYXJlbnQuZ2V0Q2hpbGRBdCgwKSlcbiAgICAgICAgY29uc29sZS5sb2cocGFyZW50LnBhcmVudC5nZXRDaGlsZEF0KDEpKVxuICAgICAgICBjb25zb2xlLmxvZyhwYXJlbnQucGFyZW50LmdldENoaWxkQXQoMikpXG4gICAgICAgIGNvbnNvbGUubG9nKHBhcmVudC5wYXJlbnQuZ2V0Q2hpbGQoMykpXG4gICAgICAgICovXG4gICAgICAgIC8vICBhY2NvcmRpb24uX3JlbW92ZVZpZXcoY2hpbGRWaWV3KTtcbiAgICAgICAgLy8gY29uc29sZS5sb2coYWNjb3JkaW9uLl9jaGlsZHJlbkNvdW50KVxuICAgICAgICAvLyBhY2NvcmRpb24ucGFyZW50Ll9yZW1vdmVWaWV3KGFjY29yZGlvbik7XG4gICAgICAgIC8vIGNoaWxkLnJcbiAgICB9XG59O1xucmVnaXN0ZXJFbGVtZW50KFwiQWNjb3JkaW9uXCIsICgpID0+IHJlcXVpcmUoXCIuLi9hY2NvcmRpb25cIikuQWNjb3JkaW9uLCBwYWdlck1ldGEpO1xucmVnaXN0ZXJFbGVtZW50KFwiQWNjb3JkaW9uSXRlbVwiLCAoKSA9PiByZXF1aXJlKFwiLi4vYWNjb3JkaW9uXCIpLkl0ZW0pO1xuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdBY2NvcmRpb24nLFxuICAgIHRlbXBsYXRlOiAnPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50Pidcbn0pXG5leHBvcnQgY2xhc3MgQWNjb3JkaW9uQ29tcG9uZW50IHtcbn1cblxuXG5cblxuLy8gaW1wb3J0IHsgRWxlbWVudFJlZiwgRGlyZWN0aXZlLCBJbnB1dCwgVGVtcGxhdGVSZWYsIFZpZXdDb250YWluZXJSZWYsIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG4vLyBpbXBvcnQgeyBBY2NvcmRpb259IGZyb20gXCIuLi9hY2NvcmRpb25cIjtcbi8vIGltcG9ydCB7U3RhY2tMYXlvdXR9IGZyb20gXCJ1aS9sYXlvdXRzL3N0YWNrLWxheW91dFwiO1xuLy8gQERpcmVjdGl2ZSh7XG4vLyAgICAgc2VsZWN0b3I6IFwiQWNjb3JkaW9uXCIsIC8vIHRzbGludDpkaXNhYmxlLWxpbmU6ZGlyZWN0aXZlLXNlbGVjdG9yXG4vLyB9KVxuLy8gZXhwb3J0IGNsYXNzIEFjY29yZGlvbkRpcmVjdGl2ZSBpbXBsZW1lbnRzIEFmdGVyVmlld0luaXQge1xuLy8gICAgIHB1YmxpYyBhY2NvcmRpb246IEFjY29yZGlvbjtcbi8vICAgICBwcml2YXRlIF9zZWxlY3RlZEluZGV4OiBudW1iZXI7XG4vLyAgICAgcHJpdmF0ZSB2aWV3SW5pdGlhbGl6ZWQ6IGJvb2xlYW47XG5cbi8vICAgICBASW5wdXQoKVxuLy8gICAgIGdldCBzZWxlY3RlZEluZGV4KCk6IG51bWJlciB7XG4vLyAgICAgICAgIHJldHVybiB0aGlzLl9zZWxlY3RlZEluZGV4O1xuLy8gICAgIH1cblxuLy8gICAgIHNldCBzZWxlY3RlZEluZGV4KHZhbHVlKSB7XG4vLyAgICAgICAgIC8vIHRoaXMuX3NlbGVjdGVkSW5kZXggPSBjb252ZXJ0VG9JbnQodmFsdWUpO1xuLy8gICAgICAgICBpZiAodGhpcy52aWV3SW5pdGlhbGl6ZWQpIHtcbi8vICAgICAgICAgICAgIHRoaXMuYWNjb3JkaW9uLnNlbGVjdGVkSW5kZXggPSB0aGlzLl9zZWxlY3RlZEluZGV4O1xuLy8gICAgICAgICB9XG4vLyAgICAgfVxuXG4vLyAgICAgY29uc3RydWN0b3IoZWxlbWVudDogRWxlbWVudFJlZikge1xuLy8gICAgICAgICB0aGlzLmFjY29yZGlvbiA9IGVsZW1lbnQubmF0aXZlRWxlbWVudDtcbi8vICAgICB9XG5cbi8vICAgICBuZ0FmdGVyVmlld0luaXQoKSB7XG4vLyAgICAgICAgIHRoaXMudmlld0luaXRpYWxpemVkID0gdHJ1ZTtcbi8vICAgICB9XG4vLyB9XG5cbi8vIEBEaXJlY3RpdmUoe1xuLy8gICAgIHNlbGVjdG9yOiBcIlthY2NvcmRpb25JdGVtXVwiIC8vIHRzbGludDpkaXNhYmxlLWxpbmU6ZGlyZWN0aXZlLXNlbGVjdG9yXG4vLyB9KVxuLy8gZXhwb3J0IGNsYXNzIEFjY29yZGlvbkl0ZW1EaXJlY3RpdmUgaW1wbGVtZW50cyBPbkluaXQge1xuLy8gICAgIHByaXZhdGUgaXRlbTogQWNjb3JkaW9uSXRlbTtcbi8vICAgICBwcml2YXRlIF9oZWFkZXJUZXh0OiBzdHJpbmc7XG5cbi8vICAgICBjb25zdHJ1Y3Rvcihcbi8vICAgICAgICAgcHJpdmF0ZSBvd25lcjogQWNjb3JkaW9uRGlyZWN0aXZlLFxuLy8gICAgICAgICBwcml2YXRlIHRlbXBsYXRlUmVmOiBUZW1wbGF0ZVJlZjxhbnk+LFxuLy8gICAgICAgICBwcml2YXRlIHZpZXdDb250YWluZXI6IFZpZXdDb250YWluZXJSZWZcbi8vICAgICApIHtcbi8vICAgICB9XG5cbi8vICAgICBASW5wdXQoXCJhY2NvcmRpb25JdGVtXCIpIGNvbmZpZzogYW55OyAvLyB0c2xpbnQ6ZGlzYWJsZS1saW5lOm5vLWlucHV0LXJlbmFtZVxuXG4vLyAgICAgQElucHV0KClcbi8vICAgICBnZXQgaGVhZGVyVGV4dCgpIHtcbi8vICAgICAgICAgcmV0dXJuIHRoaXMuX2hlYWRlclRleHQ7XG4vLyAgICAgfVxuXG4vLyAgICAgc2V0IGhlYWRlclRleHQodmFsdWU6IHN0cmluZykge1xuLy8gICAgICAgICBpZiAodGhpcy5faGVhZGVyVGV4dCAhPT0gdmFsdWUpIHtcbi8vICAgICAgICAgICAgIHRoaXMuX2hlYWRlclRleHQgPSB2YWx1ZTtcbi8vICAgICAgICAgICAgIGNvbnNvbGUubG9nKHZhbHVlKVxuLy8gICAgICAgICAgICAvLyB0aGlzLmVuc3VyZUl0ZW0oKTtcbi8vICAgICAgICAgICAgLy8gdGhpcy5pdGVtLmhlYWRlclRleHQgPSB0aGlzLl9oZWFkZXJUZXh0O1xuLy8gICAgICAgICB9XG4vLyAgICAgfVxuXG4vLyAgICAgLy8gQElucHV0KClcbi8vICAgICAvLyBnZXQgaWNvblNvdXJjZSgpIHtcbi8vICAgICAvLyAgICAgcmV0dXJuIHRoaXMuX2ljb25Tb3VyY2U7XG4vLyAgICAgLy8gfVxuXG4vLyAgICAgLy8gc2V0IGljb25Tb3VyY2UodmFsdWU6IHN0cmluZykge1xuLy8gICAgIC8vICAgICBpZiAodGhpcy5faWNvblNvdXJjZSAhPT0gdmFsdWUpIHtcbi8vICAgICAvLyAgICAgICAgIHRoaXMuX2ljb25Tb3VyY2UgPSB2YWx1ZTtcbi8vICAgICAvLyAgICAgICAgIHRoaXMuZW5zdXJlSXRlbSgpO1xuLy8gICAgIC8vICAgICAgICAgdGhpcy5pdGVtLmljb25Tb3VyY2UgPSB0aGlzLl9pY29uU291cmNlO1xuLy8gICAgIC8vICAgICB9XG4vLyAgICAgLy8gfVxuXG4vLyAgICAgcHJpdmF0ZSBlbnN1cmVJdGVtKCkge1xuLy8gICAgICAgICBpZiAoIXRoaXMuaXRlbSkge1xuLy8gICAgICAgICAgICAgdGhpcy5pdGVtID0gbmV3IEFjY29yZGlvbkl0ZW0oKTtcbi8vICAgICAgICAgfVxuLy8gICAgIH1cblxuLy8gICAgIG5nT25Jbml0KCkge1xuLy8gICAgICAgIC8vIHRoaXMuZW5zdXJlSXRlbSgpO1xuLy8gICAgICAgICAvLyBpZiAodGhpcy5jb25maWcpIHtcbi8vICAgICAgICAgLy8gICAgIHRoaXMuaXRlbS50aXRsZSA9IHRoaXMuX3RpdGxlIHx8IHRoaXMuY29uZmlnLnRpdGxlO1xuLy8gICAgICAgICAvLyAgICAgdGhpcy5pdGVtLmljb25Tb3VyY2UgPSB0aGlzLl9pY29uU291cmNlIHx8IHRoaXMuY29uZmlnLmljb25Tb3VyY2U7XG4vLyAgICAgICAgIC8vIH1cblxuLy8gICAgICAgICBjb25zdCB2aWV3UmVmID0gdGhpcy52aWV3Q29udGFpbmVyLmNyZWF0ZUVtYmVkZGVkVmlldyh0aGlzLnRlbXBsYXRlUmVmKTtcbi8vICAgICAgICAgLy8gRmlsdGVyIG91dCB0ZXh0IG5vZGVzLCBldGNcbi8vICAgICAgICAgY29uc3QgcmVhbFZpZXdzID0gdmlld1JlZi5yb290Tm9kZXMuZmlsdGVyKChub2RlKSA9PlxuLy8gICAgICAgICAgICAgbm9kZS5ub2RlTmFtZSAmJiBub2RlLm5vZGVOYW1lICE9PSBcIiN0ZXh0XCIpO1xuLy8gICAgICAgICBpZiAocmVhbFZpZXdzLmxlbmd0aCA+IDApIHtcbi8vICAgICAgICAgICAgIHRoaXMuaXRlbSA9IHJlYWxWaWV3c1swXTtcblxuLy8gICAgICAgICAgICAgLy9jb25zdCBuZXdJdGVtcyA9ICh0aGlzLm93bmVyLmFjY29yZGlvbi5pdGVtcyB8fCBbXSkuY29uY2F0KFt0aGlzLml0ZW1dKTtcbi8vICAgICAgICAgICAgIC8vdGhpcy5vd25lci5hY2NvcmRpb24uaXRlbXMgPSBuZXdJdGVtcztcblxuLy8gICAgICAgICB9XG4vLyAgICAgfVxuLy8gfVxuXG4vLyBleHBvcnQgY2xhc3MgQWNjb3JkaW9uSXRlbSBleHRlbmRzIFN0YWNrTGF5b3V0IHtcblxuLy8gfSJdfQ==