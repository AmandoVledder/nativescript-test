"use strict";
var view_1 = require("ui/core/view");
var proxy_1 = require("ui/core/proxy");
var dependency_observable_1 = require("ui/core/dependency-observable");
var platform_1 = require("platform");
var knownCollections;
(function (knownCollections) {
    knownCollections.items = "items";
})(knownCollections = exports.knownCollections || (exports.knownCollections = {}));
var AffectsLayout = platform_1.isAndroid ? dependency_observable_1.PropertyMetadataSettings.None : dependency_observable_1.PropertyMetadataSettings.AffectsLayout;
var itemsProperty = new dependency_observable_1.Property("items", "Accordion", new proxy_1.PropertyMetadata(undefined, AffectsLayout));
var selectedIndexProperty = new dependency_observable_1.Property("selectedIndex", "Accordion", new proxy_1.PropertyMetadata(undefined, dependency_observable_1.PropertyMetadataSettings.None));
itemsProperty.metadata.onSetNativeValue = function (data) {
    var accordion = data.object;
    accordion.updateItems(data.oldValue, data.newValue);
};
selectedIndexProperty.metadata.onSetNativeValue = function (data) {
    var accordion = data.object;
    accordion.indexChanged(data.newValue);
};
var Accordion = (function (_super) {
    __extends(Accordion, _super);
    function Accordion() {
        return _super.call(this) || this;
    }
    Accordion.prototype._addArrayFromBuilder = function (name, value) {
        if (name === "items") {
            this.items = value;
        }
    };
    Object.defineProperty(Accordion.prototype, "headerHeight", {
        get: function () {
            return this._headerHeight;
        },
        set: function (value) {
            this._headerHeight = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Accordion.prototype, "headerTextColor", {
        get: function () {
            return this._headerTextColor;
        },
        set: function (value) {
            this._headerTextColor = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Accordion.prototype, "headerColor", {
        get: function () {
            return this._headerColor;
        },
        set: function (value) {
            this._headerColor = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Accordion.prototype, "headerTextVerticalAlignment", {
        get: function () {
            return this._headerTextVerticalAlignment;
        },
        set: function (value) {
            this._headerTextVerticalAlignment = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Accordion.prototype, "headerTextHorizontalAlignment", {
        get: function () {
            return this._headerTextHorizontalAlignment;
        },
        set: function (value) {
            this._headerTextHorizontalAlignment = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Accordion.prototype, "headerTextSize", {
        get: function () {
            return this._headerTextSize;
        },
        set: function (value) {
            this._headerTextSize = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Accordion.prototype, "items", {
        get: function () {
            return this._getValue(Accordion.itemsProperty);
        },
        set: function (value) {
            this._setValue(Accordion.itemsProperty, value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Accordion.prototype, "selectedIndex", {
        get: function () {
            return this._getValue(Accordion.selectedIndexProperty);
        },
        set: function (value) {
            this._setValue(Accordion.selectedIndexProperty, value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Accordion.prototype, "selectedIndexes", {
        get: function () {
            return this._selectedIndexes;
        },
        set: function (indexes) {
            this._selectedIndexes = indexes;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Accordion.prototype, "allowMultiple", {
        get: function () {
            return this._allowMultiple;
        },
        set: function (value) {
            this._allowMultiple = true;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Accordion.prototype, "separatorColor", {
        get: function () {
            return this._separatorColor;
        },
        set: function (value) {
            this._separatorColor = value;
        },
        enumerable: true,
        configurable: true
    });
    return Accordion;
}(view_1.View));
Accordion.itemsProperty = itemsProperty;
Accordion.selectedIndexProperty = selectedIndexProperty;
exports.Accordion = Accordion;
