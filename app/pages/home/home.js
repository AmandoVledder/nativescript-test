var BasePage = require("../../shared/BasePage");
var topmost = require("ui/frame").topmost;

// var HomePage = function() {};
// HomePage.prototype = new BasePage();
// HomePage.prototype.constructor = HomePage;
// module.exports = new HomePage();
// Place any code you want to run when the home page loads here.
//HomePage.prototype.contentLoaded = function() {}





var createViewModel = require("../../main-view-model").createViewModel;
var Observable = require("data/observable").Observable;

function onNavigatingTo(args) {

    var page = args.object;
    page.bindingContext = createViewModel();
}

exports.onNavigatingTo = onNavigatingTo;

var timer = require("timer");

exports.onPullToRefreshInitiated = function (args) {
    var that = new WeakRef(this);
    timer.setTimeout(function () {
        var listView = args.object;
        listView.notifyPullToRefreshFinished();
    }, 1000);
};
var leftThresholdPassed = false;
var rightThresholdPassed = false;
function onCellSwiping(args) {
    var viewModule = require('ui/core/view');
    var utilsModule = require("utils/utils");
    var swipeLimits = args.data.swipeLimits;
    
    var swipeView = args['swipeView'];
    swipeLimits.threshold = swipeView.getMeasuredWidth() - 20
    swipeLimits.left =swipeView.getMeasuredWidth() - 20
    swipeLimits.right = swipeView.getMeasuredWidth() - 20
    var mainView = args['mainView'];
    var leftItem = swipeView.getViewById('mark-view');
    var rightItem = swipeView.getViewById('delete-view');
    
    if (args.data.x > swipeView.getMeasuredWidth() * 0.25 && !leftThresholdPassed) {
        console.log("Notify perform left action");
        var markLabel = leftItem.getViewById('mark-text');
        leftThresholdPassed = true;
    }
    else if (args.data.x < swipeView.getMeasuredWidth() * 0.25 && !rightThresholdPassed) {
        var deleteLabel = rightItem.getViewById('delete-text');
        console.log("Notify perform right action");
        rightThresholdPassed = true;
    }
    if (args.data.x > 0) {
        var leftDimensions = viewModule.View.measureChild(leftItem.parent, leftItem, utilsModule.layout.makeMeasureSpec(Math.abs(args.data.x), utilsModule.layout.EXACTLY), utilsModule.layout.makeMeasureSpec(mainView.getMeasuredHeight(), utilsModule.layout.EXACTLY));
        viewModule.View.layoutChild(leftItem.parent, leftItem, 0, 0, leftDimensions.measuredWidth, leftDimensions.measuredHeight);
    }
    else {
        var rightDimensions = viewModule.View.measureChild(rightItem.parent, rightItem, utilsModule.layout.makeMeasureSpec(Math.abs(args.data.x), utilsModule.layout.EXACTLY), utilsModule.layout.makeMeasureSpec(mainView.getMeasuredHeight(), utilsModule.layout.EXACTLY));
        viewModule.View.layoutChild(rightItem.parent, rightItem, mainView.getMeasuredWidth() - rightDimensions.measuredWidth, 0, mainView.getMeasuredWidth(), rightDimensions.measuredHeight);
    }
}
exports.onCellSwiping = onCellSwiping;


function onSwipeCellStarted(args) {
    var viewModule = require('ui/core/view');
    var utilsModule = require("utils/utils");
    var swipeLimits = args.data.swipeLimits;
    
    var swipeView = args['swipeView'];
    //swipeLimits.left = swipeLimits.right = swipeView.getMeasuredWidth() * 0.45; // 65% of whole width
    swipeLimits.threshold = (swipeView.getMeasuredWidth() / 2) - 20
    swipeLimits.left = swipeView.getMeasuredWidth() / 2
    swipeLimits.right = swipeView.getMeasuredWidth() / 2
}

exports.onSwipeCellStarted = onSwipeCellStarted;






